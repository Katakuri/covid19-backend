var express = require('express');
var router = express.Router();
var neo4j = require('neo4j-driver');

var driver = neo4j.driver(
        'bolt://localhost:11004',
        neo4j.auth.basic('neo4j', 'root')
        );

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


// Tous les cas 
router.get('/allNodes', (req,res,next) => {
  var session = driver.session();
  session.run("MATCH (n) WHERE n.pays = 'France' RETURN (n)").then(result => {
    console.log(result.records);
    res.send(result.records)
  }).catch(error => {
    console.log(error);
    res.send(error)
  }).then(() => session.close())
});

// Nombre total de cas et de mort dans le monde
router.get('/casesDeathsInWorld', (req,res,next) => {
  var session = driver.session();
  session.run("match(c:Covid) return sum(c.nbCas) AS NbTotaldeCas, sum(c.nbMorts) AS NombreTotalDécés").then(result => {
    console.log(result.records);
    res.send(result.records);
  }).catch(error => {
    console.log(error);
    res.send(error);
  }).then(() => session.close())
});

// Nombre de total de mort par pays

router.get('/deathsByCoutry', (req,res,next) => {
  var session = driver.session();
  session.run("/* Nombre de deceés et cas par pays */ match(c:Covid) return distinct c.pays, c.codePays as codePays, sum(c.nbMorts) as MortsTotal, sum(c.nbCas) as CasTotal order by  MortsTotal desc").then(result => {
    console.log(result.records);
    res.send(result.records);
  }).catch(error => {
    console.log(error);
    res.send(error);
  }).then(() => session.close())
});

module.exports = router;
